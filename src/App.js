import React from 'react';
import './App.css';

import Header from './components/Header';
import Factory from './components/Factory';
import Footer from './components/Footer';
import {Form} from './components/Factory';


class App extends React.Component {
  constructor(args) {
    super(args)
    this.state = { data: null }
  }
  async getData() {
    let promise = await fetch('/page.json');
    let data = await promise.json()
    return data;
  }
  async componentDidMount() {
    const data = await this.getData()
    this.setState({ data })
  }
  render() {
    if (this.state.data === null) {
      return "Загрузка..."
    }

    return (
      <div className="App">
        <div className="container">
          <Header />
          {this.state.data.components.map((component, i) => {
            return <Factory key={i} data={component} />
          })}
          <Form data={this.state.data.form}/>
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;
