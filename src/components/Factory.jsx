import React from 'react';



class GalleryComponent extends React.Component {
    render() {
        const caruselItems = []
        for (let i = 0; i < this.props.data.metadata.images.length; i += this.props.data.metadata.slidesPerView) {
            const caruselItem = []
            const end = i + this.props.data.metadata.slidesPerView
            for (let j = i; j < end; j++) {
                const link = this.props.data.metadata.images[j];
                if (link !== undefined) {
                    caruselItem.push(link)
                }
            }
            caruselItems.push(caruselItem)
        }

        return (
            <React.Fragment>
                <h1 className="titleGallery">{this.props.data.metadata.title}</h1>
                <div className="row">
                    <div className="col-sm-12 col-md-12">
                        <div id="carouselExampleControls" className="carousel slide" data-ride="carousel" data-interval="1000000">
                            <div className="carousel-inner">
                                {caruselItems.map((caruselItem, i) => {
                                    return (
                                        <div className={"carousel-item " + (i === 0 ? "active" : "")} key={i}>
                                            <div className="row">
                                                {caruselItem.map((link, j) => {
                                                    return (
                                                        <div className={"col-" + (12 / this.props.data.metadata.slidesPerView)} key={j}>
                                                            <div className="card">
                                                                <img className="card-img-top" src={link} alt="logo" />
                                                            </div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="row paginationSection">
                                <div className="col align-self-start">
                                    <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <i className="far fa-arrow-alt-circle-left"></i>
                                    </a>
                                </div>
                                <div className="col align-self-center">
                                    <ol className="carousel-indicators tabsSection">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                </div>
                                <div className="col align-self-end">
                                    <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <i className="far fa-arrow-alt-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </React.Fragment>
        )
    }
}

class GridComponent extends React.Component {
    render() {
        return (
            <div className="row">
                {this.props.data.metadata.components.map((component, i) => {
                    return <div className={"col-" + component.col} ><Factory key={i} data={component} /></div>
                })}
            </div>
        )
    }
}

class RichTextComponent extends React.Component {
    render() {
        return (
            <React.Fragment>
                <h2> {this.props.data.metadata.title} </h2>
                <div dangerouslySetInnerHTML={{ __html: this.props.data.metadata.text }} />
            </React.Fragment>
        );
    }
}

export class Form extends React.Component {
    render() {

        let hz = this.props.data;
        console.log(this.props.data.submit_button);

        return (
            <React.Fragment>
                <h2>{this.props.data.title}</h2>
                <div className="row">
                    {this.props.data.fields.map((field, i) => {
                        const className = this.props.data.field_groups[field.group]
                        return (
                            <div className={className}>
                                <div className="form-group">
                                    <label>{field.label}</label>
                                    <input class="form-control" type={field.type} name={field.name} required={field.required} />
                                </div>
                            </div>
                        )
                    })}
                </div>
            </React.Fragment>
        )
    }
}

class Factory extends React.Component {
    render() {
        switch (this.props.data.type) {
            case "GalleryComponent":
                return <GalleryComponent data={this.props.data} />

            case "GridComponent":
                return <GridComponent data={this.props.data} />

            case "RichTextComponent":
                return <RichTextComponent data={this.props.data} />
            default: return "Тестовый"

        }
    }
}
export default Factory;